//######################################
//# Devoir de : Christophe GAGNON, Johann DUFAUD
//# Titre : 	InscriptionActivity
//# Description :   Classe pour faire l'activity_inscription de l'utilisateur
//#	Date :	8 Fevrier 2016
//#######################################
package com.example.johan.liverte.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.dal.api.UserApiHelper;
import com.example.johan.liverte.model.User;
import com.example.johan.liverte.utils.HashingUtils;
import com.example.johan.liverte.utils.ImageUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InscriptionActivity
        extends AppCompatActivity {

    public static final int REQUEST_IMAGE_GALLERY = 0;
    public static final int REQUEST_IMAGE_CAPTURE = 1;


    EditText eMail, mdp, mdp2, nom, prenom, numTel;
    ImageView image;
    Bitmap imageBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        eMail = (EditText) findViewById(R.id.mail);
        mdp = (EditText) findViewById(R.id.password);
        mdp2 = (EditText) findViewById(R.id.password2);
        nom = (EditText) findViewById(R.id.nom);
        prenom = (EditText) findViewById(R.id.prenom);
        numTel = (EditText) findViewById(R.id.telephone);
        image = (ImageView) findViewById(R.id.image);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayAddImageMenu();

            }
        });

        findViewById(R.id.valider).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
    }

    // Debut de la methode pour l'image
    private void displayAddImageMenu() {
        PopupMenu popup = new PopupMenu(this, image);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.context_menu_add_image, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.from_camera:
                        Intent takeImageFromCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takeImageFromCameraIntent.resolveActivity(getPackageManager()) != null)
                            startActivityForResult(takeImageFromCameraIntent, REQUEST_IMAGE_CAPTURE);
                        return true;


                    case R.id.from_gallery:
                        Intent pickPhoto = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        );
                        startActivityForResult(pickPhoto, REQUEST_IMAGE_GALLERY);
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private boolean isEmailMatchingRegex() {
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(eMail.getText().toString());
        return m.matches();
    }

    private User getUser() {
        return new User(
                0L,
                eMail.getText().toString(),
                HashingUtils.md5(mdp.getText().toString()),
                prenom.getText().toString(),
                nom.getText().toString(),
                numTel.getText().toString());
    }

    private boolean areInputsValids() {
        return !(eMail.getText().toString().equals("") ||
                mdp.getText().toString().equals("") ||
                mdp2.getText().toString().equals("") ||
                nom.getText().toString().equals("") ||
                prenom.getText().toString().equals("") ||
                numTel.getText().toString().equals(""));
    }

    private boolean arePasswordMatching() {
        return mdp.getText().toString().equals(mdp2.getText().toString());
    }


    private void registerUser() {

        if (!isEmailMatchingRegex()) {
            Toast.makeText(this, "Erreur email",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if (!areInputsValids()) {
            Toast.makeText(
                    this,
                    "Vous avez oublie de remplir une case du formulaire",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (!arePasswordMatching()) {
            Toast.makeText(
                    this,
                    "Erreur les deux mots de passe sont incorrect",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if (imageBitmap == null)
            setDefaultImage();

        UserApiHelper.SubscribeUser(this, getUser(), imageBitmap, new UserApiHelper.UserCallBack() {
            @Override
            public void onSuccess(User user) {
                ((Liverte) getApplicationContext()).setLoggedUser(user);
                Toast.makeText(
                        getApplicationContext(),
                        "User ajouté avec succès \nBienvenue " + prenom.getText().toString(),
                        Toast.LENGTH_LONG
                ).show();
                finish();
            }

            @Override
            public void onError() {
                Toast.makeText(
                        getApplicationContext(),
                        "Cette adresse mail existe deja.",
                        Toast.LENGTH_LONG
                ).show();

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bitmap returnedImage = null;

        switch (requestCode) {
            case REQUEST_IMAGE_GALLERY:
                if (resultCode == Activity.RESULT_OK) {
                    Uri imageUri = data.getData();
                    returnedImage = ImageUtils.getBitmapFromUri(InscriptionActivity.this, imageUri);
                }
                break;

            case REQUEST_IMAGE_CAPTURE:
                if (data == null)
                    return;
                Bundle extras = data.getExtras();
                returnedImage = (Bitmap) extras.get("data");
                break;
        }

        if (returnedImage != null)
            setImageBitmap(returnedImage);
    }

    private void setDefaultImage() {
        setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.book));
    }

    private void setImageBitmap(Bitmap image) {
        imageBitmap = ImageUtils.scaleBitmap(
                image,
                Math.round(150 * ((Liverte) getApplicationContext()).getDisplayMetrics().scaledDensity),
                Math.round(150 * ((Liverte) getApplicationContext()).getDisplayMetrics().scaledDensity)
        );
        this.image.setImageBitmap(imageBitmap);

    }

}
