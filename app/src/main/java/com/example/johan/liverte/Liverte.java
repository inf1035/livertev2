package com.example.johan.liverte;

import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.example.johan.liverte.dal.SharedPreferenceHelper;
import com.example.johan.liverte.dal.sqlite.SQLiteHelper;
import com.example.johan.liverte.model.User;
import com.facebook.stetho.Stetho;

public class Liverte extends Application {

    private DisplayMetrics DISPLAY_METRICS;
    private SQLiteHelper database;
    private User loggedUser;

    public DisplayMetrics getDisplayMetrics() {
        return DISPLAY_METRICS;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        database = SQLiteHelper.getInstance(getApplicationContext());
        DISPLAY_METRICS = getScreenMetrics();
    }

    public DisplayMetrics getScreenMetrics() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    public SQLiteHelper getDatabase() {
        return database;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(User user) {
        SharedPreferenceHelper.saveCurrentUser(this, user);
        this.loggedUser = user;

    }
}