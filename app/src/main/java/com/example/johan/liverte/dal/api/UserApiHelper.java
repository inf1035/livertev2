package com.example.johan.liverte.dal.api;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.johan.liverte.model.User;
import com.example.johan.liverte.utils.HashingUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

/**
 * Created by David on 2016-10-09.
 */

public class UserApiHelper {

    public static void LoginUser(final Context ctx, String email, String password, final UserCallBack callback) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, ApiHelper.BAS_URL + "login_user.php?email=" + email + "&pass=" + HashingUtils.cypher(password),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(User.FromJson(response));
                        } catch (JSONException e) {
                            callback.onError();
                        }

                        // Do something with the response
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError();
                    }
                });

        ApiHelper.getInstance(ctx).addToRequestQueue(request);

    }

    public static void SubscribeUser(final Context ctx, final User user, Bitmap image, final UserCallBack callback) {

        HashMap<String, Object> multipart = new HashMap<>();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        assert image != null;
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);

        multipart.put("image", stream.toByteArray());
        multipart.put("data", user.toString());

        MultipartRequest mpr = new MultipartRequest(ApiHelper.BAS_URL + "add_user.php?",
                null,
                multipart,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            response.getString("succes");
                            callback.onSuccess(user);
                        } catch (JSONException e) {
                            callback.onError();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError();
                    }
                });


        ApiHelper.getInstance(ctx).addToRequestQueue(mpr);


    }

    public interface UserCallBack {
        void onSuccess(User user);

        void onError();
    }


}
