package com.example.johan.liverte.dal;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.activity.fragments.MarketDetailFragment;
import com.example.johan.liverte.dal.api.ApiHelper;
import com.example.johan.liverte.dal.sqlite.DatabaseModel;
import com.example.johan.liverte.model.Book;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by tourfade on 16-04-19.
 */
public class Synchronizer {
    private Liverte app;

    public Synchronizer(Liverte app) {
        this.app = app;
    }


    public void initBookMarket(final MarketLoaderCallback callBack) {
        initBookMarket(MarketDetailFragment.PUBLIC, callBack);
    }

    public void initBookMarket(int marketType, final MarketLoaderCallback callBack) {
        SharedPreferences pref = app.getSharedPreferences("account", Context.MODE_PRIVATE);
        final long lastUpdate = pref.getLong("lastupdated", 0);
        String email = null;
        if (marketType == MarketDetailFragment.PRIVATE) {
            email = app.getLoggedUser().getMail();
        }


        getLocalBookMarket(callBack, email);
        if (app.getDatabase().isDirty() || (((new Date()).getTime() - lastUpdate) / 1000L) > 60) {
            getRmoteBookMarket(callBack, email);
        }
    }

    public void getLocalBookMarket(final MarketLoaderCallback callBack, final String email) {

        (new AsyncTask<Void, Void, ArrayList<Book>>() {

            @Override
            protected ArrayList<Book> doInBackground(Void[] params) {
                return app.getDatabase().getAllLivres(email);
            }

            @Override
            protected void onPostExecute(ArrayList<Book> o) {
                callBack.onMarketRead(o);
            }
        }).execute();
    }


    public void getRmoteBookMarket(final MarketLoaderCallback callBack, final String email) {

        String body = prepareRequest().toString();
        //Log.i("Synchronizer",body);
        JsonArrayRequest jor = new JsonArrayRequest(Request.Method.POST,
                ApiHelper.BAS_URL + "get_livre.php",
                body,
                new Response.Listener<JSONArray>() {
                    public void onResponse(JSONArray response) {


                        try {
                            String toRemove = response.getString(1);
                            app.getDatabase().removeLivres(toRemove.trim());
                            JSONArray toAdd = response.getJSONArray(0);


                            ContentValues cv = null;


                            for (int i = 0; i < toAdd.length(); i++) {
                                JSONArray object = toAdd.getJSONArray(i);
                                cv = new ContentValues();
                                cv.put(DatabaseModel.Livre.F_USER_MAIL, object.getString(7));
                                cv.put(DatabaseModel.Livre.F_REMOTE_ID, object.getLong(0));
                                cv.put(DatabaseModel.Livre.F_DATA, object.toString());
                                cv.put(DatabaseModel.Livre.F_STATUS, 1);
                                cv.put(DatabaseModel.Livre.LAST_UPDATED, object.getString(object.length() - 1));
                                app.getSharedPreferences("account", Context.MODE_PRIVATE).
                                        edit().putLong("lastupdated", new Date().getTime())
                                        .commit();
                                app.getDatabase().save(cv);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        getLocalBookMarket(callBack, email);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        );
        ApiHelper.getInstance(app).addToRequestQueue(jor);
    }

    public JSONObject prepareRequest() {
        return new JSONObject(app.getDatabase().getIDs());

    }


    public interface MarketLoaderCallback {
        void onMarketRead(ArrayList<Book> liste);
    }
}
