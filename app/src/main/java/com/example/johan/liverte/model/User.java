package com.example.johan.liverte.model;

import com.example.johan.liverte.dal.api.ApiHelper;
import com.example.johan.liverte.utils.HashingUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class User {

    // Keys de la serialisation JSON
    public static final String MAIL = "MAIL";
    public static final String MDP = "MDP";
    public static final String NOM = "NOM";
    public static final String PRENOM = "PRENOM";
    public static final String NUMTEL = "NUMTEL";

    private long id;
    private String mail;
    private String md5Password;
    private String firstName;
    private String lastName;
    private String phoneNumber;

    public User(long id, String mail,
                String md5Password, String firstName,
                String lastName, String phoneNumber) {
        this.id = id;
        this.mail = mail;
        this.md5Password = md5Password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    public String getImgurl() {
        return ApiHelper.BAS_URL + "uploads/" + id + ".png";
    }

    public String getMail() {
        return mail;
    }

    public String getMd5Password() {
        return md5Password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
     return toJson().toString();
    }


    public static User FromJson(JSONObject object) throws JSONException {
        return new User(
                object.getLong("_ID"),
                object.getString("_MAIL"),
                object.getString("_MDP"),
                object.getString("_NOM"),
                object.getString("_PRENOM"),
                object.getString("_PHONE"));
    }

    public JSONObject toJson(){
        JSONObject jo = new JSONObject();
        try {
            jo.put(MAIL, mail);
            jo.put(MDP, HashingUtils.cypher(md5Password));
            jo.put(NOM, lastName);
            jo.put(PRENOM, firstName);
            jo.put(NUMTEL, phoneNumber);
            return jo;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

}