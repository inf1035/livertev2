import android.content.Context;

import com.example.johan.liverte.R;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

/**
 * Created by David on 2016-11-28.
 */

public class MockedContextUnitTest {

    private static final String FAKE_STRING = "HELLO WORLD";

    @Mock
    Context mMockContext;

    @Test
    public void readStringFromContext_LocalizedString() {
        // Given a mocked Context injected into the object under test...
        Mockito.when(mMockContext.getString(R.string.info_livre)).thenReturn(FAKE_STRING);

        // ClassUnderTest myObjectUnderTest = new ClassUnderTest(mMockContext);

        // ...when the string is returned from the object under test...
        // String result = myObjectUnderTest.getHelloWorldString();

        // ...then the result should be the expected one.
       //  assertThat(result, is(FAKE_STRING));
    }
}
