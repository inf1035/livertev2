package com.example.johan.liverte.dal.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.johan.liverte.model.Book;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SQLiteHelper extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 2;
    // Database Name
    private static final String DATABASE_NAME = "Liverte.DB";
    private static SQLiteHelper helper;
    private SQLiteDatabase db;
    private boolean dirty;


    private SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
    }

    public static synchronized SQLiteHelper getInstance(Context context) {
        if (helper == null) {
            helper = new SQLiteHelper(context);
        }
        return helper;
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseModel.Utilisateur.CREATE_TABLE);
        db.execSQL(DatabaseModel.Livre.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL(DatabaseModel.Livre.DROP_TABLE);
        db.execSQL(DatabaseModel.Utilisateur.DROP_TABLE);

        // Create tables again
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DatabaseModel.Livre.DROP_TABLE);
        db.execSQL(DatabaseModel.Utilisateur.DROP_TABLE);
        onCreate(db);
    }


    // Getting All LIVRE
    public ArrayList<Book> getAllLivres(String usermail) {
        ArrayList<Book> books = new ArrayList<>();
        Log.i("myemail", "" + usermail);
        Cursor cursor = db.query(
                DatabaseModel.Livre.TABLE_NAME,
                new String[]{DatabaseModel.Livre._ID, DatabaseModel.Livre.F_DATA},//projection
                DatabaseModel.Livre.F_STATUS + " > ? " +
                        (usermail == null ? "" : " AND " + DatabaseModel.Livre.F_USER_MAIL + " = ?"),
                usermail == null ? new String[]{"0"} : new String[]{"0", usermail},//=
                null,//GROUPBY
                null,//HAVING
                null);//ORDERBY

        try {
            if (cursor.moveToFirst()) {
                do {
                    books.add(new Book(cursor.getLong(0), new JSONArray(cursor.getString(1))));
                } while (cursor.moveToNext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cursor.close();
        return books;
    }

    public void insertNew(Book book) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseModel.Livre.F_REMOTE_ID, 0);
        cv.put(DatabaseModel.Livre.F_DATA, book.toJson().toString());
        cv.put(DatabaseModel.Livre.F_STATUS, 1);
        cv.put(DatabaseModel.Livre.LAST_UPDATED, new Date().toString());
        db.insert(DatabaseModel.Livre.TABLE_NAME, null, cv);
    }

    public void save(ContentValues v) {
        db.insert(DatabaseModel.Livre.TABLE_NAME, null, v);
    }

    public void removeLivres(String ids) {
        if (ids.length() > 0) {
            db.execSQL("DELETE  FROM " + DatabaseModel.Livre.TABLE_NAME +
                    " WHERE " + DatabaseModel.Livre.F_REMOTE_ID + " IN(" + ids + ")");
        }
    }

    public void removeLivre(long id) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseModel.Livre.F_STATUS, -1);
        db.update(DatabaseModel.Livre.TABLE_NAME,
                cv,
                DatabaseModel.Livre._ID + " = ?",
                new String[]{String.valueOf(id)}
        );

    }

    public HashMap<String, HashMap<String, String>> getIDs() {
        HashMap<String, String> oldItems = new HashMap<>();
        Cursor cursor = db.query(
                DatabaseModel.Livre.TABLE_NAME,
                new String[]{DatabaseModel.Livre.F_REMOTE_ID, DatabaseModel.Livre.LAST_UPDATED},//projection
                DatabaseModel.Livre.F_REMOTE_ID + " > ? AND " + DatabaseModel.Livre.F_STATUS + " > ?",
                new String[]{"0", "0"},//
                null,//GROUPBY
                null,//HAVING
                null);//ORDERBY

        if (cursor.moveToFirst()) {
            do {
                oldItems.put("" + cursor.getLong(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();

        HashMap<String, String> newItems = new HashMap<>();
        cursor = db.query(
                DatabaseModel.Livre.TABLE_NAME,
                new String[]{DatabaseModel.Livre._ID, DatabaseModel.Livre.F_DATA},//projection
                DatabaseModel.Livre.F_REMOTE_ID + " = ?",
                new String[]{"0"},//=
                null,//GROUPBY
                null,//HAVING
                null);//ORDERBY
        if (cursor.moveToFirst()) {
            do {
                newItems.put("" + cursor.getLong(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();

        HashMap<String, String> removedItems = new HashMap<>();
        cursor = db.query(
                DatabaseModel.Livre.TABLE_NAME,
                new String[]{DatabaseModel.Livre._ID, DatabaseModel.Livre.F_REMOTE_ID},//projection
                DatabaseModel.Livre.F_STATUS + " <= ?",
                new String[]{"0"},//=
                null,//GROUPBY
                null,//HAVING
                null);//ORDERBY
        if (cursor.moveToFirst()) {
            do {
                removedItems.put("" + cursor.getLong(0), cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();

        HashMap<String, HashMap<String, String>> results = new HashMap<>();
        results.put("oldItems", oldItems);
        results.put("newItems", newItems);
        results.put("removedItems", removedItems);

        return results;

    }


    public boolean isDirty() {

        Cursor cursor = db.query(
                DatabaseModel.Livre.TABLE_NAME,
                new String[]{DatabaseModel.Livre._ID},//projection
                DatabaseModel.Livre.F_STATUS + " < ?",
                new String[]{"0"},//=
                null,//GROUPBY
                null,//HAVING
                null);//ORDERBY
        Log.i("Dirty ?", String.valueOf(cursor.getCount() > 0));
        return cursor.getCount() > 0;
    }
}