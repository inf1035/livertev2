package com.example.johan.liverte.activity.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.johan.liverte.R;
import com.example.johan.liverte.dal.ContactProviderAccessor;
import com.example.johan.liverte.model.Book;

public class InfoLivreDetailFragment extends Fragment {

    private static final String ARG_TRANSITION_NAME = "transition_name";


    public static final String FRAGMENT_ID_INFO_LIVRE = InfoLivreDetailFragment.class.getCanonicalName();
    private Book currentBook;
    private FloatingActionButton addToContactButton;

    public void setLivre(Book tempSelected) {
        currentBook = tempSelected;
    }

    private String _imageTransitionName;


    // TODO: Rename and change types and number of parameters
    public static InfoLivreDetailFragment newInstance(String imageTransitionName) {
        InfoLivreDetailFragment fragment = new InfoLivreDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TRANSITION_NAME, imageTransitionName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            _imageTransitionName = getArguments().getString(ARG_TRANSITION_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_info_livre_detail, container, false);

        ImageView image = (ImageView)view.findViewById(R.id.image);

        image.setTransitionName(_imageTransitionName);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (currentBook != null) {
            ((TextView) view.findViewById(R.id.nom_livre)).setText(currentBook.getNom());
            ((TextView) view.findViewById(R.id.auteur_livre)).setText(currentBook.getAuteur());
            ((TextView) view.findViewById(R.id.prix_occasion_livre)).setText(currentBook.getPrixOccasion());
            ((TextView) view.findViewById(R.id.prix_neuf_livre)).setText(currentBook.getPrixNeuf());
            ((TextView) view.findViewById(R.id.etat_livre)).setText(currentBook.getEtat().toString());
            ((TextView) view.findViewById(R.id.langue_livre)).setText(currentBook.getLangue());
            ((TextView) view.findViewById(R.id.mail_livre)).setText(currentBook.getMailUtilisateur());
        }

        // ADD TO CONTACT
        addToContactButton = (FloatingActionButton) view.findViewById(R.id.add_to_contact);
        addToContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactProviderAccessor.AddBookToContact(currentBook, getActivity());
            }
        });

    }


}
