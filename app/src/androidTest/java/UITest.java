import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.content.ComponentName;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.johan.liverte.R;
import com.example.johan.liverte.activity.ConnexionActivity;
import com.example.johan.liverte.activity.MainLivreActivity;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.action.ViewActions.*;

/**
 * Created by David on 2016-11-28.
 */


@RunWith(AndroidJUnit4.class)
@LargeTest
public class UITest {

    private String _username;
    private String _password;

    @Rule
    public ActivityTestRule<ConnexionActivity> mActivityRule = new ActivityTestRule<>(
            ConnexionActivity.class);

    @Before
    public void before() {
        _username = "a@a.fr";
        _password = "a";

        Intents.init();
    }

    @Test
    public void changeText_sameActivity() {

        onView(withId(R.id.username))
                .perform(ViewActions.typeText(_username), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.password_main))
                .perform(ViewActions.typeText(_password), ViewActions.closeSoftKeyboard());


        Espresso.onView(withId(R.id.connexion)).perform(ViewActions.click());

        Intents.intended(IntentMatchers.hasComponent(MainLivreActivity.class.getName()));

    }



    @After
    public void after() {
                Intents.release();
    }
}
