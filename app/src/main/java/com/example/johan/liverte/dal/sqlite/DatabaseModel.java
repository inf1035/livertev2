package com.example.johan.liverte.dal.sqlite;

import android.provider.BaseColumns;

public class DatabaseModel {

    public static final String AS_TEXT = "  TEXT ";
    public static final String AS_INT = "  INTEGER ";
    public static final String AS_INT_PRIMARY = "  INTEGER PRIMARY KEY ";

    public static class Utilisateur implements BaseColumns {
        public static final String TABLE_NAME = "Utilisateur";

        public static final String MAIL = "_mail";
        public static final String MDP = "_password";
        public static final String NAME_UTILISATEUR = "_nom";
        public static final String PRENOM = "_prenom";
        public static final String PH_NO = "_phone_number";
        public static final String IMAGE = "_image";


        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + _ID + AS_INT_PRIMARY + " ,"
                + MAIL + AS_TEXT + " ,"
                + MDP + AS_TEXT + " ,"
                + NAME_UTILISATEUR + AS_TEXT + " ,"
                + PRENOM + AS_TEXT + " ,"
                + PH_NO + AS_TEXT + " ,"
                + IMAGE + AS_TEXT
                + "); ";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + "; ";

    }


    public static class Livre implements BaseColumns {
        public static final String TABLE_NAME = "Book";

        public static final String F_REMOTE_ID = "_remote_id";
        public static final String F_DATA = "_json_data";
        public static final String F_STATUS = "_status";
        public static final String LAST_UPDATED = "_last_updated";
        public static final String F_USER_MAIL = "_user_email";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + _ID + AS_INT_PRIMARY + " ,"
                + F_REMOTE_ID + AS_INT + " ,"
                + F_DATA + AS_TEXT + " ,"
                + F_STATUS + AS_INT + " ,"
                + F_USER_MAIL + AS_TEXT + " , "
                + LAST_UPDATED + AS_TEXT + "); ";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + "; ";

    }
}