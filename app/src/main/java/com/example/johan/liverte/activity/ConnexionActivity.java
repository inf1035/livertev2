//######################################
//# Devoir de : Christophe GAGNON, Johann DUFAUD
//# Titre : 	ConnexionActivity
//# Description :   Programme principal, ConnexionActivity et accès à l'activity_inscription
//#	Date :	8 Fevrier 2016
//#######################################
package com.example.johan.liverte.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.dal.api.UserApiHelper;
import com.example.johan.liverte.model.User;
import com.example.johan.liverte.utils.HashingUtils;

public class ConnexionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // boutton
        findViewById(R.id.enregistrer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), InscriptionActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.connexion).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connect();
            }
        });

    }


    public void connect() {

        String mailS = ((EditText) findViewById(R.id.username)).getText().toString().replace(" ", "%20");
        String mdpS = HashingUtils.md5(((EditText) findViewById(R.id.password_main)).getText().toString());

        UserApiHelper.LoginUser(this, mailS, mdpS, new UserApiHelper.UserCallBack() {
            @Override
            public void onSuccess(User user) {
                ((Liverte) getApplicationContext()).setLoggedUser(user);
                Intent intent = new Intent(getApplicationContext(), MainLivreActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                finish();
            }

            @Override
            public void onError() {
                Toast.makeText(
                        getApplicationContext(),
                        "Erreur dans le mail ou le mot de passe",
                        Toast.LENGTH_LONG
                ).show();

            }
        });

    }
}
