package com.example.johan.liverte.dal;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.model.User;

public class SharedPreferenceHelper {

    private static final String KEY_ACCOUNT = "account";

    private static final String KEY_ACCOUNT_ID = "id";
    private static final String KEY_ACCOUNT_EMAIL = "MAIL";
    private static final String KEY_ACCOUNT_PASSWORD = "MPD";
    private static final String KEY_ACCOUNT_LASTNAME = "NOM";
    private static final String KEY_ACCOUNT_FIRSTNAME = "PRENOM";
    private static final String KEY_ACCOUNT_PHONENUMBER = "NUMTeL";

    public static void saveCurrentUser(Liverte liverte, User user) {
        SharedPreferences.Editor e = liverte.getSharedPreferences(KEY_ACCOUNT, Context.MODE_PRIVATE).edit();
        e.putLong(KEY_ACCOUNT_ID, user.getId());
        e.putString(KEY_ACCOUNT_EMAIL, user.getMail());
        e.putString(KEY_ACCOUNT_PASSWORD, user.getMd5Password());
        e.putString(KEY_ACCOUNT_LASTNAME, user.getLastName());
        e.putString(KEY_ACCOUNT_FIRSTNAME, user.getFirstName());
        e.putString(KEY_ACCOUNT_PHONENUMBER, user.getPhoneNumber());
        e.commit();
    }

    public static User getUser(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_ACCOUNT, Context.MODE_PRIVATE);
        return new User(
                pref.getLong(KEY_ACCOUNT_ID, -1L),
                pref.getString(KEY_ACCOUNT_EMAIL, null),
                pref.getString(KEY_ACCOUNT_PASSWORD, null),
                pref.getString(KEY_ACCOUNT_LASTNAME, null),
                pref.getString(KEY_ACCOUNT_FIRSTNAME, null),
                pref.getString(KEY_ACCOUNT_PHONENUMBER, null)
        );
    }


}
