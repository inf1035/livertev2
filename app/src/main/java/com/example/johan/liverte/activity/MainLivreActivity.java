package com.example.johan.liverte.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;

import com.example.johan.liverte.R;
import com.example.johan.liverte.activity.fragments.MarketDetailFragment;

public class MainLivreActivity extends NavigationDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_main_livre);


        initNavigationDrawer();

        NavigationView navigationView = (NavigationView) findViewById(R.id.my_drawer);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public void onResume() {
        super.onResume();
        showMarket(MarketDetailFragment.PUBLIC);
    }
}

