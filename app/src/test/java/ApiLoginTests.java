import android.content.Context;

import com.example.johan.liverte.R;
import com.example.johan.liverte.dal.api.ApiHelper;
import com.example.johan.liverte.dal.api.UserApiHelper;
import com.example.johan.liverte.model.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by David on 2016-11-28.
 */
@RunWith(MockitoJUnitRunner.class)
public class ApiLoginTests {

    private static final String FAKE_STRING = "HELLO WORLD";

    @Mock
    Context mMockContext;

    @Test
    public void loginToApi_UserNotNull() {


        final User[] loggedUser = new User[1];

        UserApiHelper.LoginUser(mMockContext, "a@a.fr", "a", new UserApiHelper.UserCallBack() {
            @Override
            public void onSuccess(User user) {
                loggedUser[0] = user;
            }

            @Override
            public void onError() { }
        });

        assertNotNull(loggedUser[0]);
    }


}
