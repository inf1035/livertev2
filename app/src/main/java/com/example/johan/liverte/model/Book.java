//######################################
//# Devoir de : Christophe GAGNON, Johann DUFAUD
//# Titre : 	Book
//# Description :   Objet Book avec
//#	Date :	8 Fevrier 2016
//#######################################
package com.example.johan.liverte.model;

import org.json.JSONArray;
import org.json.JSONException;

import javax.annotation.Nullable;

public class Book {
    private long localid;
    private String nom;
    private String auteur;
    private String prixOccasion;
    private String prixNeuf;
    private String lang;
    private Condition etat;
    private String mailUtilisateur;
    private String latitude;
    private String longitude;

    public Book(String nom, String auteur, String prixOccasion, String prixNeuf, String lang, Condition etat, String mailUtilisateur, @Nullable String Latitude, @Nullable String Longitude) {
        this.nom = nom;
        this.auteur = auteur;
        this.prixOccasion = prixOccasion;
        this.prixNeuf = prixNeuf;
        this.lang = lang;
        this.etat = etat;
        this.mailUtilisateur = mailUtilisateur;
        if (Latitude != null)
            this.latitude = Latitude;
        if (Longitude != null)
            this.longitude = Longitude;
        localid = 0L;
    }

    public Book(long id, JSONArray jsonObject) throws JSONException {
        localid = id;
        nom = jsonObject.getString(1);
        auteur = jsonObject.getString(2);
        prixOccasion = jsonObject.getString(3);
        prixNeuf = jsonObject.getString(4);
        lang = jsonObject.getString(5);
        etat = Condition.getFromLabel(jsonObject.getString(6));
        mailUtilisateur = jsonObject.getString(7);
        latitude = jsonObject.getString(8);
        longitude = jsonObject.getString(9);
    }

    public JSONArray toJson() {
        JSONArray ja = new JSONArray();
        ja.put(nom);
        ja.put(auteur);
        ja.put(prixOccasion);
        ja.put(prixNeuf);
        ja.put(lang);
        ja.put(etat);
        ja.put(mailUtilisateur);
        ja.put(latitude);
        ja.put(longitude);
        return ja;
    }

    public long getLocalID() {
        return localid;
    }

    //    public static Book getFromJson(JSONObject object){
//
//        return new Book(
//                object.getString(1),
//                object.getString(2),
//                object.getString(3),
//                object.getString(4),
//                object.getString(5),
//                object.getString(6),
//                object.getString(7),
//                object.getString(8),
//                object.getString(9)
//                );
//
//    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getPrixOccasion() {
        return prixOccasion;
    }

    public void setPrixOccasion(String prixOccasion) {
        this.prixOccasion = prixOccasion;
    }

    public String getPrixNeuf() {
        return prixNeuf;
    }

    public void setPrixNeufn(String prixNeuf) {
        this.prixNeuf = prixNeuf;
    }

    public String getLangue() {
        return lang;
    }

    public void setLangue(String lang) {
        this.lang = lang;
    }

    public Condition getEtat() {
        return etat;
    }

    public void setEtat(Condition etat) {
        this.etat = etat;
    }

    public String getMailUtilisateur() {
        return mailUtilisateur;
    }

    public void setMailUtilisateur(String mailUtilisateur) {
        this.mailUtilisateur = mailUtilisateur;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String Latitude) {
        this.latitude = Latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String Longitude) {
        this.longitude = Longitude;
    }


}
