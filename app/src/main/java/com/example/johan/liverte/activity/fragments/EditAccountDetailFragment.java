//######################################
//# Devoir de : Christophe GAGNON, Johann DUFAUD
//# Titre : 	Modify Account
//# Description :   Modifier le user utilisateur
//#	Date :	8 Fevrier 2016
//#######################################
package com.example.johan.liverte.activity.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.dal.api.ApiHelper;
import com.example.johan.liverte.model.User;
import com.example.johan.liverte.utils.ImageUtils;

import java.io.IOException;

/**
 * Created by Johann on 29/02/2016.
 */
public class EditAccountDetailFragment extends Fragment {


    public static final int REQUEST_IMAGE_GALLERY = 0;
    public static final int REQUEST_IMAGE_CAPTURE = 1;


    private EditText mdpMod, nomMod, prenomMod, numTelMod;
    private NetworkImageView imageMod;

    public static Bitmap getBitmapFromUri(Context context, Uri imageUri) {
        try {
            return MediaStore.Images.Media.getBitmap(
                    context.getContentResolver(),
                    imageUri
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_account_detail, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        User currentUser = ((Liverte) getActivity().getApplicationContext()).getLoggedUser();
        //Bouttons
        //ConnexionActivity SQLiteHelper
        mdpMod = (EditText) v.findViewById(R.id.mdp_modify);
        nomMod = (EditText) v.findViewById(R.id.modify_surname);
        nomMod.setText(currentUser.getFirstName());
        prenomMod = (EditText) v.findViewById(R.id.modify_name);
        prenomMod.setText(currentUser.getLastName());
        numTelMod = (EditText) v.findViewById(R.id.modify_phone);
        numTelMod.setText(currentUser.getPhoneNumber());
        imageMod = (NetworkImageView) v.findViewById(R.id.modify_image);
        imageMod.setImageUrl(currentUser.getImgurl(), ApiHelper.getInstance(getContext()).getImageLoader());


        v.findViewById(R.id.buttonSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        imageMod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.buttonSave:
                        Toast.makeText(
                                getActivity(),
                                "Non indenté sur le serveur désolé ....",
                                Toast.LENGTH_LONG
                        ).show();
                        //modifyTheAccount();
                        break;

                    case R.id.modify_image:
                        ajoutImage();
                        break;

                }
            }
        });
    }

    // Debut methode image
    private void ajoutImage() {
        showPopupMenu();
    }

    private void showPopupMenu() {
        PopupMenu popup = new PopupMenu(getActivity(), imageMod);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.context_menu_add_image, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.from_camera:
                        takeImageFromCamera();
                        return true;

                    case R.id.from_gallery:
                        pickImageFromGalery();
                        break;
                }
                return false;
            }
        });
        popup.show();
    }


    private void takeImageFromCamera() {
        Intent takeImageFromCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takeImageFromCameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takeImageFromCameraIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void pickImageFromGalery() {
        Intent pickPhoto = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        );
        startActivityForResult(pickPhoto, REQUEST_IMAGE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_IMAGE_GALLERY: {
                if (resultCode == Activity.RESULT_OK) {
                    Uri imageUri = data.getData();

                    Bitmap bitmap = getBitmapFromUri(getActivity(), imageUri);
                    bitmap = ImageUtils.scaleBitmap(
                            bitmap,
                            Math.round(150 * ((Liverte) getActivity().getApplicationContext()).getDisplayMetrics().scaledDensity),
                            Math.round(150 * ((Liverte) getActivity().getApplicationContext()).getDisplayMetrics().scaledDensity)
                    );

                    imageMod.setImageBitmap(bitmap);

                }
                break;
            }
            case REQUEST_IMAGE_CAPTURE: {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                // on réduit sa taille pour qu'elle entre parfaitement dans mon ImageView (ici 72dp)
                imageBitmap = ImageUtils.scaleBitmap(
                        imageBitmap,
                        Math.round(150 * ((Liverte) getActivity().getApplicationContext()).getDisplayMetrics().scaledDensity),
                        Math.round(150 * ((Liverte) getActivity().getApplicationContext()).getDisplayMetrics().scaledDensity)
                );

                // on met l'image dans l'ImageView
                imageMod.setImageBitmap(imageBitmap);


                break;
            }
        }
    }
}