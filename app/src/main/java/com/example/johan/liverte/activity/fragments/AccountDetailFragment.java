package com.example.johan.liverte.activity.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.dal.api.ApiHelper;
import com.example.johan.liverte.model.User;


public class AccountDetailFragment extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_account_detail, container, false);
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        v.findViewById(R.id.button_modifier).setOnClickListener(this);
        displayUserInfos(((Liverte) getActivity().getApplicationContext()).getLoggedUser());
    }

    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = getActivity().getFragmentManager();

        EditAccountDetailFragment accountEditor = (EditAccountDetailFragment) fragmentManager.findFragmentByTag(EditAccountDetailFragment.class.getName());

        if (accountEditor == null) {
            accountEditor = new EditAccountDetailFragment();
        }
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.addToBackStack(EditAccountDetailFragment.class.getName());
        ft.replace(R.id.main_content_panel, accountEditor, EditAccountDetailFragment.class.getName())
                .commit();
    }


    private void displayUserInfos(User user) {
        ((TextView) getView().findViewById(R.id.nom_Amodifier)).setText(user.getFirstName());
        ((TextView) getView().findViewById(R.id.prenom_Amodifier)).setText(user.getLastName());
        ((TextView) getView().findViewById(R.id.mail_Amodifier)).setText(user.getMail());
        ((TextView) getView().findViewById(R.id.tel_Amodifier)).setText(user.getPhoneNumber());
        ((NetworkImageView) getView().findViewById(R.id.image_compte)).setImageUrl(user.getImgurl(), ApiHelper.getInstance(getContext()).getImageLoader());
    }
}

