package com.example.johan.liverte.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.activity.fragments.AccountDetailFragment;
import com.example.johan.liverte.activity.fragments.AnnoncerDetailFragment;
import com.example.johan.liverte.activity.fragments.MapsDetailfragment;
import com.example.johan.liverte.activity.fragments.MarketDetailFragment;
import com.example.johan.liverte.activity.fragments.SettingsPreferencesFragment;
import com.example.johan.liverte.dal.api.ApiHelper;
import com.example.johan.liverte.model.User;

/**
 * Created by David on 2016-10-09.
 */

public abstract class NavigationDrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    protected DrawerLayout drawer;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private NetworkImageView image;
    private TextView headerMail, headerNom;
    private User user;

    public void initNavigationDrawer() {

        user = ((Liverte) getApplicationContext()).getLoggedUser();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.my_drawer);
        View navView = navigationView.getHeaderView(0);
        headerMail = (TextView) navView.findViewById(R.id.header_mail);
        headerNom = (TextView) navView.findViewById(R.id.header_nom);
        image = (NetworkImageView) navView.findViewById(R.id.image_header);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));

        headerMail.setText(user.getMail());
        headerNom.setText(user.getFirstName() +
                " " + user.getLastName());
        image.setImageUrl(user.getImgurl(), ApiHelper.getInstance(this).getImageLoader());

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }


    private <T> void showFragment(Class<T> fragClass) {
        try {
            Fragment fragment = getFragmentManager().findFragmentByTag(fragClass.getName());

            if (fragment == null)
                fragment = (Fragment) fragClass.newInstance();

            FragmentTransaction ft = getFragmentManager().beginTransaction();

            if (ft.isAddToBackStackAllowed())
                ft.addToBackStack(null);

            // ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.fade_out);
            ft.replace(R.id.main_content_panel, fragment, fragClass.getName()).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void showMarket(int type) {
        MarketDetailFragment marketFragment = (MarketDetailFragment) getFragmentManager().findFragmentByTag(MarketDetailFragment.class.getName() + type);
        if (marketFragment == null)
            marketFragment = new MarketDetailFragment();

        marketFragment.setMarketType(type);
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        if (ft.isAddToBackStackAllowed())
            ft.addToBackStack(null);

        //ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.fade_out);
        ft.replace(R.id.main_content_panel, marketFragment, MarketDetailFragment.class.getName() + type);
        ft.commit();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.user:
                showFragment(AccountDetailFragment.class);
                break;

            case R.id.vendre:
                showFragment(AnnoncerDetailFragment.class);
                break;

            case R.id.marche:
                showMarket(MarketDetailFragment.PUBLIC);
                break;

            case R.id.carte:
                showFragment(MapsDetailfragment.class);
                break;

            case R.id.annonces:
                showMarket(MarketDetailFragment.PRIVATE);
                break;

            case R.id.settings:
                showFragment(SettingsPreferencesFragment.class);
                break;

            case R.id.exit:
                Intent intent = new Intent(getApplicationContext(), ConnexionActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

}
