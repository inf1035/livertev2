package com.example.johan.liverte.activity.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.dal.Synchronizer;
import com.example.johan.liverte.model.Book;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsDetailfragment extends MapFragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Synchronizer syncher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater,
                (FrameLayout) inflater.inflate(R.layout.fragment_maps_detail, container),
                savedInstanceState);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        syncher = new Synchronizer((Liverte) getActivity().getApplication());
    }

    @Override
    public void onResume() {
        super.onResume();
        getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // On pointe sur Trois Rivieres
        LatLng TR = new LatLng(46.35409104, -72.55069699);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(TR, 11)); // Camera sur Trois-Rivieres avec un zoom de 11
        syncher.initBookMarket(new Synchronizer.MarketLoaderCallback() {
            @Override
            public void onMarketRead(ArrayList<Book> liste) {

                for (Book book : liste) {
                    try {

                        LatLng test = new LatLng(Double.parseDouble(book.getLatitude()),
                                Double.parseDouble(book.getLongitude()));
                        mMap.addMarker(new MarkerOptions().position(test).title("Book : " + book.getNom()).snippet("Contact : " + book.getMailUtilisateur()));

                        //Si il m'y a pas de donnes de localisation alors
                    } catch (Exception e) {

                    }
                }
            }
        });

    }


}
