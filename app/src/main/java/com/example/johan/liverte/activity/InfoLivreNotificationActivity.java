package com.example.johan.liverte.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.johan.liverte.R;
import com.example.johan.liverte.model.Book;

/**
 * Created by Johann on 16-11-15.
 */

public class InfoLivreNotificationActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onNewIntent(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent){
        Bundle extras = intent.getExtras();
        if (extras !=null)
            if(extras.containsKey("bookName")){
                setContentView(R.layout.notification_addbook);

                // Récuperation des infos du book
                ((TextView) findViewById(R.id.nom_livre)).setText(extras.getString("bookName"));
                ((TextView) findViewById(R.id.auteur_livre)).setText(extras.getString("bookAuteur"));
                ((TextView) findViewById(R.id.prix_occasion_livre)).setText(extras.getString("bookPrixOccasion"));
                ((TextView) findViewById(R.id.prix_neuf_livre)).setText(extras.getString("bookPrixNeuf"));
                ((TextView) findViewById(R.id.etat_livre)).setText(extras.getString("bookEtat"));
                ((TextView) findViewById(R.id.langue_livre)).setText(extras.getString("bookLangue"));
                ((TextView) findViewById(R.id.mail_livre)).setText(extras.getString("bookMailUtilisateur"));

            }
    }
}
