//######################################
//# Devoir de : Christophe GAGNON, Johann DUFAUD
//# Titre : 	Image Utils
//# Description : Méthodes pour l'image du compte
//#	Date :	8 Fevrier 2016
//#######################################
package com.example.johan.liverte.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageUtils {
    /**
     * Permet d'avoir le chemin d'une Uri.
     *
     * @param context    [...]
     * @param contentUri l'Uri en question.
     * @return le chemin absolu de cette Uri. Par exemple, pour une photo :
     * /storage/emulated/0/DCIM/100ANDRO/DSC_00xxxx.JPG
     */
    public static String getPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index;
            if (cursor != null) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Faire une rotation d'une image Bitmap en fonction de certains attributs.
     *
     * @param bitmap        image.
     * @param exifInterface attributs de l'image.
     * @return une image ayant la bonne orientation.
     */
    public static Bitmap rotateBitmap(Bitmap bitmap, ExifInterface exifInterface) {
        Matrix matrix = new Matrix();

        int orientation = exifInterface
                .getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(
                    bitmap,
                    0,
                    0,
                    bitmap.getWidth(),
                    bitmap.getHeight(),
                    matrix,
                    true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Crée une image Bitmap à partir d'une Uri
     *
     * @param context  [...]
     * @param imageUri l'Uri en question.
     * @return l'image en format Bitmap.
     */
    public static Bitmap getBitmapFromUri(Context context, Uri imageUri) {
        try {
            return MediaStore.Images.Media.getBitmap(
                    context.getContentResolver(),
                    imageUri
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Réduit une image bitmap.
     *
     * @param bitmap l'image à réduire.
     * @param width  la nouvelle largeur souhaitée.
     * @param height la nouvelle hauteur souhaitée.
     * @return l'image réduite.
     */
    public static Bitmap scaleBitmap(Bitmap bitmap, int width, int height) {
        return Bitmap.createScaledBitmap(
                bitmap,
                width,
                height,
                false
        );
    }

    public static byte[] getByteArrayFromBitmap(Bitmap image) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        assert image != null;
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    /**
     * Avoir les paramètres d'une image JPG.
     *
     * @param imagePath le chemin (path) de l'image dans le système android
     * @return ExifInterface : les paramètres (orientation, flash actif, etc etc ..)
     */
    public static ExifInterface getExifInterfaceFromImageUri(String imagePath) {
        ExifInterface exifInterface = null;

        try {
            exifInterface = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return exifInterface;
    }
}
