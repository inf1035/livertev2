package com.example.johan.liverte.utils;

import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by David on 2016-10-10.
 */

public class HashingUtils {

    public static String md5(String in) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(in.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String cypher(String params) {

        MCrypt mcrypt = new MCrypt();
        String r = null;
        try {
            r = mcrypt.bytesToHex(mcrypt.encrypt(params));

            return r;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private static class MCrypt {

        private String iv = "FOXALPHADELTALIM";
        private IvParameterSpec ivspec;
        private SecretKeySpec keyspec;
        private Cipher cipher;

        private String SecretKey = "dmiuqtrhiver2015";//16 caracteres

        public MCrypt() {
            ivspec = new IvParameterSpec(iv.getBytes());

            keyspec = new SecretKeySpec(SecretKey.getBytes(), "AES");

            try {
                cipher = Cipher.getInstance("AES/CBC/NoPadding");
            } catch (NoSuchAlgorithmException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        public byte[] encrypt(String text) throws Exception {
            if (text == null || text.length() == 0)
                throw new Exception("Empty string");

            byte[] encrypted = null;

            try {
                cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);

                encrypted = cipher.doFinal(padString(text).getBytes());
            } catch (Exception e) {
                throw new Exception("[encrypt] " + e.getMessage());
            }

            return encrypted;
        }

        public String bytesToHex(byte[] data) {
            if (data == null) {
                return null;
            }

            int len = data.length;
            String str = "";
            for (int i = 0; i < len; i++) {
                if ((data[i] & 0xFF) < 16)
                    str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
                else
                    str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
            }
            return str;
        }

        private String padString(String source) {
            char paddingChar = ' ';
            int size = 16;
            int x = source.length() % size;
            int padLength = size - x;

            for (int i = 0; i < padLength; i++) {
                source += paddingChar;
            }

            return source;
        }


    }
}
