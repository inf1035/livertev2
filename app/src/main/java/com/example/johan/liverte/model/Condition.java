package com.example.johan.liverte.model;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class Condition {

    public static final Condition New = new Condition("Neuf");
    public static final Condition Excellent = new Condition("Comme Neuf");
    public static final Condition Good = new Condition("Bon Etat");
    public static final Condition Medium = new Condition("Moyen");
    public static final Condition Bad = new Condition("Mauvais");
    public static final Condition Noted = new Condition("Annote");

    private String label;

    private Condition(String label) {
        this.label = label;
    }

    public static Condition getFromLabel(String label) {
        for (Condition cond : getAll())
            if (cond.label.toLowerCase().equals(label.toLowerCase()))
                return cond;
        return null;
    }

    public static ArrayList<String> getAllString() {
        ArrayList<String> conditionsString = new ArrayList<String>();
        for (Condition cond : getAll())
            conditionsString.add(cond.label);
        return conditionsString;
    }

    public static ArrayList<Condition> getAll() {
        ArrayList<Condition> conditions = new ArrayList<Condition>();
        for (Field f : Condition.class.getDeclaredFields())
            if (Modifier.isStatic(f.getModifiers()))
                try {
                    Condition condition = (Condition) f.get(null);
                    if (condition != null)
                        conditions.add(condition);
                } catch (Exception e) {

                }
        return conditions;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }
}

