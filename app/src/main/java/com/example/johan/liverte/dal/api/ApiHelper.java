package com.example.johan.liverte.dal.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;


public class ApiHelper {

    //public static  final String BAS_URL = "http://dmilinux.uqtr.ca/mi709/php/";
    // public static  final String BAS_URL = "http://198.37.170.10/api/";
    public static final String BAS_URL = "http://192.168.1.114/api/";
    protected static Context mCtx;
    protected static ApiHelper mInstance;
    protected RequestQueue mRequestQueue;
    protected ImageLoader mImageLoader;

    protected ApiHelper(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized ApiHelper getInstance(Context context) {
        if (mInstance == null)
            mInstance = new ApiHelper(context);
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null)
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


    public ImageLoader getImageLoader() {
        return mImageLoader;
    }


}
