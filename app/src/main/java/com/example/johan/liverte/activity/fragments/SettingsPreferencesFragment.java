package com.example.johan.liverte.activity.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.example.johan.liverte.R;

public class SettingsPreferencesFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

}