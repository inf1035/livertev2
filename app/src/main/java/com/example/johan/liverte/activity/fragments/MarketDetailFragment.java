package com.example.johan.liverte.activity.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.adapter.LivreAdapter;
import com.example.johan.liverte.dal.Synchronizer;
import com.example.johan.liverte.model.Book;

import java.util.ArrayList;


public class MarketDetailFragment extends Fragment {

    public static int PRIVATE = 1;
    public static int PUBLIC = 2;
    private Synchronizer synchronizer;
    private LivreAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView liste;
    private Liverte app;
    private int marketType = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_market_detail, container, false);

        app = (Liverte) getActivity().getApplication();
        liste = (RecyclerView) v.findViewById(R.id.liste);
        liste.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        liste.setLayoutManager(mLayoutManager);

        synchronizer = new Synchronizer(app);

        adapter = new LivreAdapter(getActivity());
        adapter.setOncardClickListener(new LivreAdapter.ListCardClickListener() {
            @Override
            public void onCardClick(final View v,  final int position) {
                final ImageView img = (ImageView) v.findViewById(R.id.image);
                if (marketType == PUBLIC) {
                    showDetails(img, position);
                } else {
                    PopupMenu popup = new PopupMenu(getActivity(), v);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.context_menu_info_livre, popup.getMenu());

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_info:
                                    showDetails(img, position);
                                    return true;

                                case R.id.action_delete:
                                    app.getDatabase().removeLivre(adapter.getLivre(position).getLocalID());
                                    reloadMarket();
                                    return true;

                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                }
            }
        });
        adapter.setLivres(new ArrayList<Book>());
        liste.setAdapter(adapter);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadMarket();
    }

    private void reloadMarket() {
        final ProgressBar progress = (ProgressBar) getView().findViewById(R.id.progressBar);
        progress.setVisibility(View.VISIBLE);
        synchronizer.initBookMarket(marketType, new Synchronizer.MarketLoaderCallback() {

            public void onMarketRead(ArrayList<Book> liste) {
                adapter.setLivres(liste);
                progress.setVisibility(View.GONE);
            }
        });
    }


    private void showDetails(ImageView image, int position) {


        FragmentManager fragmentManager = getFragmentManager();
        InfoLivreDetailFragment infoLivre = InfoLivreDetailFragment.newInstance(image.getTransitionName());
        infoLivre.setLivre(adapter.getLivre(position));
        infoLivre.setSharedElementEnterTransition(new DetailsTransition());
        // infoLivre.setEnterTransition(new Fade());
        // setExitTransition(new Explode());
        this.setSharedElementReturnTransition(new DetailsTransition());

        fragmentManager.beginTransaction()
                .addSharedElement(image, image.getTransitionName())
                .replace(R.id.main_content_panel, infoLivre)
                .addToBackStack(null)
                .commit();


    }


    public void setMarketType(int marketType) {
        this.marketType = marketType;
    }


    class DetailsTransition extends TransitionSet {
        public DetailsTransition() {
            setOrdering(ORDERING_TOGETHER);
            addTransition(new ChangeBounds()).
                    addTransition(new ChangeTransform()).
                    addTransition(new ChangeImageTransform());
        }
    }
}
