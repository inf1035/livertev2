//######################################
//# Devoir de : Christophe GAGNON, Johann DUFAUD
//# Titre : 	LivreAdapter
//# Description :   Classe dérivé de RecyclerView pour la liste de livre
//#	Date :	8 Fevrier 2016
//#######################################
package com.example.johan.liverte.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.johan.liverte.R;
import com.example.johan.liverte.model.Book;

import java.util.ArrayList;

public class LivreAdapter extends RecyclerView.Adapter<LivreAdapter.Holder> {

    private static ListCardClickListener cardClickListener;
    private final Context context;
    int lastpos = -1;
    private ArrayList<Book> items;
    private SharedPreferences sharedPref;

    public LivreAdapter(Context context) {
        this.context = context;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

    }

    public Book getLivre(int position) {

        return items.get(position);
    }

    public void setLivres(ArrayList<Book> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_livre, parent, false);

        return new Holder(v);
    }


    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (items != null && items.size() != 0) {
            holder.nom.setTextColor(Color.parseColor(sharedPref.getString("pref_key_item_title_color", "#ffffb046")));
            holder.nom.setText(items.get(position).getNom());

            holder.auteur.setText(items.get(position).getAuteur());
            holder.prix.setText(items.get(position).getPrixOccasion() + "$");
            holder.etat.setText(items.get(position).getEtat().toString());
            holder.langue.setText(items.get(position).getLangue());
            holder.image.setVisibility(sharedPref.getBoolean("pref_key_display_icons", true) ? View.VISIBLE : View.GONE);
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.book, null));
            holder.image.setTransitionName("image_" + position);

        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOncardClickListener(ListCardClickListener listener) {

        cardClickListener = listener;
    }

    private void setAnimation(View itemView, int position) {
        if (position > lastpos) {
            Animation anim = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            itemView.startAnimation(anim);
            lastpos = position;
        }
    }

//    @Override
//    public void onViewDetachedFromWindow(Holder holder) {
//        holder.clearAnimation();
//    }

    public interface ListCardClickListener {
        void onCardClick(View v, int position);
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView etat, nom, auteur, prix, langue;
        public ImageView image;

        public Holder(View itemView) {
            super(itemView);

            nom = (TextView) itemView.findViewById(R.id.nom);
            auteur = (TextView) itemView.findViewById(R.id.auteur);
            prix = (TextView) itemView.findViewById(R.id.prix);
            image = (ImageView) itemView.findViewById(R.id.image);

            etat = (TextView) itemView.findViewById(R.id.etat);
            langue = (TextView) itemView.findViewById(R.id.langue);
            image = (ImageView) itemView.findViewById(R.id.image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            cardClickListener.onCardClick(v, getAdapterPosition());
        }

        public void clearAnimation() {
            itemView.clearAnimation();
        }
    }
}
