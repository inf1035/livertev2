package com.example.johan.liverte.activity.fragments;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.SyncStateContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.johan.liverte.Liverte;
import com.example.johan.liverte.R;
import com.example.johan.liverte.activity.InfoLivreNotificationActivity;
import com.example.johan.liverte.dal.api.BookApiHelper;
import com.example.johan.liverte.model.Book;
import com.example.johan.liverte.model.Condition;
import com.example.johan.liverte.model.User;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import android.content.Context;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;

public class AnnoncerDetailFragment extends Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private EditText nom_livre, auteur, prixOccaz, prixNeuf, latitude, longitude;
    private Spinner etat;
    private RadioGroup lang;
    private RadioButton langB;
    private LocationRequest locationRequest;
    private GoogleApiClient mGoogleApiClient;
    private User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_annoncer_detail, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        v.findViewById(R.id.valider_livre).setOnClickListener(this); //Methode pour le boutton
        user = ((Liverte) getActivity().getApplicationContext()).getLoggedUser();
        // Localisation
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(3000);
        locationRequest.setFastestInterval(1000);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        ArrayAdapter<String> stringArrayAdapter =
                new ArrayAdapter<>(getContext(),
                        android.R.layout.simple_spinner_dropdown_item,
                        Condition.getAllString());
        Spinner spinner =
                (Spinner) v.findViewById(R.id.etat);
        spinner.setAdapter(stringArrayAdapter);

        nom_livre = (EditText) v.findViewById(R.id.titre_livre);
        auteur = (EditText) v.findViewById(R.id.auteurs);
        prixOccaz = (EditText) v.findViewById(R.id.prix_occaz);
        prixNeuf = (EditText) v.findViewById(R.id.prix_neuf);
        etat = (Spinner) v.findViewById(R.id.etat);
        lang = (RadioGroup) v.findViewById(R.id.lang);
        latitude = (EditText) v.findViewById(R.id.latitude);
        longitude = (EditText) v.findViewById(R.id.longitude);
        langB = (RadioButton) getView().findViewById(lang.getCheckedRadioButtonId());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.valider_livre: {
                vendre();
                break;
            }
        }
    }


    private Book generateBook() {
        return new Book(nom_livre.getText().toString(),
                auteur.getText().toString(),
                prixOccaz.getText().toString(),
                prixNeuf.getText().toString(),
                langB.getText().toString(),
                Condition.getFromLabel(etat.getSelectedItem().toString()),
                user.getMail(), latitude.getText().toString(), longitude.getText().toString());
    }

    private boolean areInputsValids() {
        return !(nom_livre.getText().toString().equals("") || auteur.getText().toString().equals("") || prixOccaz.getText().toString().equals("")
                || prixNeuf.getText().toString().equals("") || etat.getSelectedItem().toString().equals("") || langB.getText().toString().equals(""));
    }

    private void vendre() {

        // Si il manque une info dans la case du formulaire
        if (!areInputsValids()) {
            Toast.makeText(
                    getContext(),
                    "Vous avez oublie de remplir une case du formulaire",
                    Toast.LENGTH_LONG
            ).show();
            return;
        }


        BookApiHelper.PublishBook(getContext(), generateBook(), new BookApiHelper.BookCallback() {
            @Override
            public void onSuccess(Book book) {
                Toast.makeText(
                        getContext(),
                        "Book : " + book.getNom() + " ajouté avec succes",
                        Toast.LENGTH_LONG
                ).show();
                addNotification(book);
                showMarket(MarketDetailFragment.PUBLIC);
            }

            @Override
            public void onError() {
                Toast.makeText(
                        getContext(),
                        "Erreur",
                        Toast.LENGTH_LONG
                ).show();

            }
        });
    }


    protected void showMarket(int type) {
        MarketDetailFragment marketFragment = (MarketDetailFragment) getFragmentManager().findFragmentByTag(MarketDetailFragment.class.getName() + type);
        if (marketFragment == null)
            marketFragment = new MarketDetailFragment();

        marketFragment.setMarketType(type);
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        if (ft.isAddToBackStackAllowed())
            ft.addToBackStack(null);

        //ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.fade_out);
        ft.replace(R.id.main_content_panel, marketFragment, MarketDetailFragment.class.getName() + type);
        ft.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    //Service de localisation
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, new com.google.android.gms.location.LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double latitudeD, longitudeD;
                String latitudeS = null, longitudeS = null;
                Geocoder gcd = new Geocoder(getContext(), Locale.getDefault());
                List<Address> addresses;
                try {
                    addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    if (addresses.size() > 0) {
                        latitudeD = addresses.get(0).getLatitude();
                        longitudeD = addresses.get(0).getLongitude();
                        latitudeS = Double.toString(latitudeD);
                        longitudeS = Double.toString(longitudeD);

                    } else {
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                latitude.setText(latitudeS);
                longitude.setText(longitudeS);
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    // Methode qui creer une notification quand un livre est crée
    private void addNotification(Book book) {
        NotificationCompat.Builder builder =
                 new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.drawable.book)
                        .setContentTitle("Livre ajouté avec succes")
                        .setContentText(""+book.getNom())
                        .setAutoCancel(true);

        Intent notificationIntent = new Intent(getActivity(), InfoLivreNotificationActivity.class);
        // Ajout des infos du livre dans le intent
        notificationIntent.putExtra("bookName", book.getNom());
        notificationIntent.putExtra("bookAuteur", book.getAuteur());
        notificationIntent.putExtra("bookPrixOccasion", book.getPrixOccasion());
        notificationIntent.putExtra("bookPrixNeuf", book.getPrixNeuf());
        notificationIntent.putExtra("bookEtat", book.getEtat().toString());
        notificationIntent.putExtra("bookLangue", book.getLangue());
        notificationIntent.putExtra("bookMailUtilisateur", book.getMailUtilisateur());

        PendingIntent contentIntent = PendingIntent.getActivity(getContext(), 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        int mNotificationId = 001;
        // Add as notification
        NotificationManager manager = (NotificationManager)getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);

        manager.notify(mNotificationId, builder.build());
    }
}